<?php
App::uses('AppModel', 'Model');
/**
 * Webpage Model
 *
 * @property Category $Category
 * @property Keyword $Keyword
 */
class Webpage extends AppModel {
/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'content' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'The content can not be left blank.'
			)
		),'title' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'The title can not be left blank.'
			)
		),
		'seo_title' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'The seo title can not be left blank.'
			)
		),
		'seo_description' => array(
			'notempty' => array(
				'rule' => array('notempty'),
				'message' => 'The seo description can not be left blank.'
			)
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Category' => array(
			'className' => 'Category',
			'foreignKey' => 'category_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasAndBelongsToMany associations
 *
 * @var array
 */
	public $hasAndBelongsToMany = array(
		'Keyword' => array(
			'className' => 'Keyword',
			'joinTable' => 'keywords_webpages',
			'foreignKey' => 'webpage_id',
			'associationForeignKey' => 'keyword_id',
			'unique' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		)
	);

}
