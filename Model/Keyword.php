<?php

App::uses('AppModel', 'Model');

/**
 * Keyword Model
 *
 * @property Webpage $Webpage
 */
class Keyword extends AppModel
{
	/**
	 * Display Field
	 * 
	 * @var string
	 */
	public $displayField = 'word';
	
	/**
	 * Validation rules
	 *
	 * @var array
	 */
	public $validate = array(
		'word' => array(
			'notempty' => array(
				'rule' => array('notempty'),
			//'message' => 'Your custom message here',
			//'allowEmpty' => false,
			//'required' => false,
			//'last' => false, // Stop validation after this rule
			//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
			'unique' => array(
				'rule' => array('isUnique')
			)
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

	/**
	 * hasAndBelongsToMany associations
	 *
	 * @var array
	 */
	public $hasAndBelongsToMany = array(
		'Webpage' => array(
			'className' => 'Webpage',
			'joinTable' => 'keywords_webpages',
			'foreignKey' => 'keyword_id',
			'associationForeignKey' => 'webpage_id',
			'unique' => true,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'finderQuery' => '',
			'deleteQuery' => '',
			'insertQuery' => ''
		)
	);

}
?>