<?php
/* Keyword Test cases generated on: 2012-01-14 22:47:53 : 1326599273*/
App::uses('Keyword', 'Model');

/**
 * Keyword Test Case
 *
 */
class KeywordTestCase extends CakeTestCase {
/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array('app.keyword', 'app.webpage', 'app.keywords_webpage');

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();

		$this->Keyword = ClassRegistry::init('Keyword');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Keyword);

		parent::tearDown();
	}

}
