<?php
/* Webpage Test cases generated on: 2012-01-14 22:48:16 : 1326599296*/
App::uses('Webpage', 'Model');

/**
 * Webpage Test Case
 *
 */
class WebpageTestCase extends CakeTestCase {
/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array('app.webpage', 'app.category', 'app.keyword', 'app.keywords_webpage');

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();

		$this->Webpage = ClassRegistry::init('Webpage');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Webpage);

		parent::tearDown();
	}

}
