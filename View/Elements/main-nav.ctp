<?php
$navArray = array(
	array(
		"text" => "fraud",
		"controller" => "categories",
		"action" => "view",
		"params" => "16",
		"submenu" => array(
			array(
				"text" => "Elrte Test Freed",
				"controller" => "webpages",
				"action" => "display",
				"params" => "elrte_test_freed",
			),
		)
	),
	array(
		"text" => "General",
		"controller" => "categories",
		"action" => "view",
		"params" => "1",
		"submenu" => array(
			array(
				"text" => "Test",
				"controller" => "webpages",
				"action" => "display",
				"params" => "test",
			),
		)
	),
	array(
		"text" => "Normal",
		"controller" => "categories",
		"action" => "view",
		"params" => "2",
		"submenu" => array(
			array(
				"text" => "Home",
				"controller" => "webpages",
				"action" => "display",
				"params" => "home",
			),
		)
	),
);
echo $this->Nav->build($navArray, "jqueryslidemenu grid_24", "main-nav");
echo $this->Js->buffer("jqueryslidemenu.buildmenu('main-nav', arrowimages);");
?>
