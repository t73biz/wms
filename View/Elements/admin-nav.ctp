<?php
	$navArray = array(
		array(
			'text'		=> 'Categories',
			'controller'=> 'categories',
			'action'	=> 'index',
			'submenu'	=> array(
				array(
					'text'		=> 'Add New Category',
					'controller'=> 'categories',
					'action'	=> 'add'
				),
				array(
					'text'		=> 'List Categories',
					'controller'=> 'categories',
					'action'	=> 'index'
				)
			)
		),
		array(
			'text' => 'Keywords',
			'controller'=> 'keywords',
			'action'	=> 'index',
			'submenu'	=> array(
				array(
					'text'		=> 'Add New Keyword',
					'controller'=> 'keywords',
					'action'	=> 'add'
				),
				array(
					'text'		=> 'List Keywords',
					'controller'=> 'keywords',
					'action'	=> 'index'
				)
			)
		),
		array(
			'text' => 'Webpages',
			'controller'=> 'webpages',
			'action'	=> 'index',
			'submenu'	=> array(
				array(
					'text'		=> 'Add New Webpage',
					'controller'=> 'webpages',
					'action'	=> 'add'
				),
				array(
					'text'		=> 'List Webpages',
					'controller'=> 'webpages',
					'action'	=> 'index'
				)
			)
		)
	);
	echo $this->Nav->build($navArray, 'jqueryslidemenu grid_24', 'admin-nav');
	echo $this->Js->buffer('jqueryslidemenu.buildmenu("admin-nav", arrowimages);');
?>