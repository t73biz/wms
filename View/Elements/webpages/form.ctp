<?php
	$seo_title = "";
	if(isset($this->request->data))
	{
		$seo_title = $this->request->data['Webpage']['seo_title'];
	}
	echo $this->Form->input(
		'seo_title',
		array(
			'type' => 'hidden'
		)
	);
	echo $this->Html->div(
		'grid_18 alpha omega fake-frame rounded-3  left-column',
		$this->Form->input(
			'title',
			array(
				'class' => 'grid_18 alpha omega',
				'label' => array(
					'class' => 'grid_18'
				),
				'div' => array(
					'class' => 'grid_18 alpha omega'
				)
			)
		).
		$this->Form->input(
			'seo_title_display',
			array(
				'class' => 'grid_18 alpha omega',
				'label' => array(
					'class' => 'grid_18',
					'text'	=> 'Seo Title'
				),
				'div' => array(
					'class' => 'grid_18 alpha omega'
				),
				'value' => $seo_title,
				'disabled' => true
			)
		) . 
		$this->Form->input(
			'seo_description',
			array(
				'class' => 'grid_18 alpha omega',
				'label' => array(
					'class' => 'grid_18'
				),
				'div' => array(
					'class' => 'grid_18 alpha omega'
				)
			)
		)  .
		$this->Form->input(
			'custom_url',
			array(
				'class' => 'grid_18 alpha omega',
				'label' => array(
					'class' => 'grid_18'
				),
				'div' => array(
					'class' => 'grid_18 alpha omega'
				)
			)
		)  .
		$this->Form->input(
			'content',
			array(
				'class' => 'grid_18 alpha omega',
				'label' => array(
					'class' => 'grid_6'
				),
				'div' => array(
					'class' => 'grid_18 alpha omega',
					'id'	=> 'WebpageContentDiv'
				)
			)
		)
		
	);
	echo $this->Html->div(
		'grid_6 alpha omega fake-frame rounded-3 right-column',
		$this->Form->input(
			'Keyword',
			array(
				'class' => 'grid_3 alpha omega',
				'label' => array(
					'class' => 'grid_6',
					'text'	=> 'Keywords' .
					$this->Form->input(
						'KeywordAdd',
						array(
							'label' => FALSE,
							'class' => 'grid_3 alpha omega',
							'div'	=> FALSE
						)
					) .
					$this->Html->div(
						'grid_1 alpha omega',
						$this->Html->tag(
							'span',
							'&nbsp;',
							array(
								'class' => 'ui-icon ui-icon-disk ui-state-disabled',
								'id'	=> 'KeywordAddIcon'
							)
						)
					)
				),
				'div' => array(
					'class' => 'grid_6 alpha omega',
					'id'	=> 'KeywordKeywordDiv'
				),
				'multiple' => 'checkbox'
			)
		) .
		$this->Html->tag(
			'div',
			'&nbsp;',
			array(
				'class' => 'clear'
			)
		) .
		$this->Form->input(
			'category_add',
			array(
				'label' => FALSE,
				'class' => 'grid_3 omega',
				'div'	=> FALSE
			)
		).
		$this->Html->div(
			'grid_1 alpha omega',
			$this->Html->tag(
				'span',
				'&nbsp;',
				array(
					'class' => 'ui-icon ui-icon-disk ui-state-disabled',
					'id'	=> 'CategoryAdd'
				)
			)
		) .
		$this->Form->input(
			'category_id',
			array(
				'options' => $categories,
				'empty' => array(
					0 => '(Choose One)'
				),
				'class' => 'grid_6 alpha omega',
				'label' => array(
					'class' => 'grid_2'
				),
				'div' => FALSE
			)
		) .
		$this->Form->input(
			'status',
			array(
				'class' => 'grid_6 alpha omega',
				'label' => array(
					'class' => 'grid_2 push_4'
				),
				'div' => array(
					'class' => 'grid_6 alpha omega'
				),
				'type' => 'select',
				'options' => array(
					'draft' => 'draft',
					'review' => 'review',
					'publish' => 'publish'
				)
			)
		)
	);
	
	$options = array(
		'label' => __('Save Page'),
		'value' => __('Save Page'),
		'div' => array(
			'class' => 'grid_4',
		)
	);
	echo $this->Html->div('clear','&nbsp;');
	echo $this->Form->end($options);
	
	echo $this->Html->script("elrte", array('inline' => false));
	echo $this->Html->script("i18n/elrte.en", array('inline' => false));
	$script  = <<< EOL

		var opts = {
			cssClass		: 'el-rte ui-resizable grid_18 alpha omega',
			doctype			: '<!DOCTYPE html>',
			lang			: 'en',
			styleWithCSS	: true,
			height			: 400,
			toolbar			: 'maxi'
		};
		$('#WebpageContent').elrte(opts);
		$('#WebpageTitle').blur(
			function()
			{
				$.post(
					'http://wms.localhost/webpages/setSeoTitle',
					{
						value : $(this).val()
					},
					function(data) {
						$('#WebpageSeoTitle').val(data);
						$('#WebpageSeoTitleDisplay').val(data);
					}
				);
			}
		);

		$('#WebpageCategoryAdd').keyup(
			function()
			{
				if($('#WebpageCategoryAdd').val() != '')
				{
					$('#CategoryAdd').removeClass('ui-state-disabled');
				}
				else
				{
					$('#CategoryAdd').addClass('ui-state-disabled');
				}
			}
		);

		$('#WebpageKeywordAdd').keyup(
			function()
			{
				if($('#WebpageKeywordAdd').val() != '')
				{
					$('#KeywordAddIcon').removeClass('ui-state-disabled');
				}
				else
				{
					$('#KeywordAddIcon').addClass('ui-state-disabled');
				}
			}
		);

		$('#CategoryAdd').click(
			function()
			{
				if($('#WebpageCategoryAdd').val() != '')
				{
					$.post(
						'http://wms.localhost/categories/add',
						{
							value : $('#WebpageCategoryAdd').val()
						},
						function(data) {
							$('#WebpageCategoryAdd').val('');
							$('#WebpageCategoryId').html(data);
							$('#CategoryAdd').addClass('ui-state-disabled');
						}
					);
				}
				else
				{
					$('#CategoryAdd').addClass('ui-state-disabled');
				}
			}
		);

		$('#KeywordAddIcon').click(
			function()
			{
				if($('#WebpageKeywordAdd').val() != '')
				{
					$.post(
						'http://wms.localhost/keywords/add',
						{
							value : $('#WebpageKeywordAdd').val()
						},
						function(data) {
							$('#WebpageKeywordAdd').val('');
							$('#KeywordKeywordDiv').append(data);
							$('#KeywordAddIcon').addClass('ui-state-disabled');
						}
					);
				}
				else
				{
					$('#KeywordAddIcon').addClass('ui-state-disabled');
				}
			}
		);
EOL;
	$this->Js->buffer($script);
?>