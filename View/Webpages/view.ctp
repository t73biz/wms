<div class="webpages view">
<h2><?php  echo __('Webpage');?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($webpage['Webpage']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Category'); ?></dt>
		<dd>
			<?php echo $this->Html->link($webpage['Category']['name'], array('controller' => 'categories', 'action' => 'view', $webpage['Category']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Title'); ?></dt>
		<dd>
			<?php echo h($webpage['Webpage']['title']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Content'); ?></dt>
		<dd>
			<?php echo h($webpage['Webpage']['content']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Seo Title'); ?></dt>
		<dd>
			<?php echo h($webpage['Webpage']['seo_title']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Seo Description'); ?></dt>
		<dd>
			<?php echo h($webpage['Webpage']['seo_description']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($webpage['Webpage']['created']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<?php if (!empty($webpage['Keyword'])):?>
<div class="related">
	<h3><?php echo __('Related Keywords');?></h3>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Word'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($webpage['Keyword'] as $keyword): ?>
		<tr>
			<td><?php echo $keyword['id'];?></td>
			<td><?php echo $keyword['word'];?></td>
			<td><?php echo $keyword['created'];?></td>
			<td><?php echo $keyword['modified'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'keywords', 'action' => 'view', $keyword['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'keywords', 'action' => 'edit', $keyword['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'keywords', 'action' => 'delete', $keyword['id']), null, __('Are you sure you want to delete # %s?', $keyword['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
</div>
<?php endif; ?>