<div class="keywords view">
<h2><?php  echo __('Keyword');?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($keyword['Keyword']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Word'); ?></dt>
		<dd>
			<?php echo h($keyword['Keyword']['word']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($keyword['Keyword']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($keyword['Keyword']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="related">
	<h3><?php echo __('Related Webpages');?></h3>
	<?php if (!empty($keyword['Webpage'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Category Id'); ?></th>
		<th><?php echo __('Title'); ?></th>
		<th class="actions"><?php echo __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($keyword['Webpage'] as $webpage): ?>
		<tr>
			<td><?php echo $webpage['id'];?></td>
			<td><?php echo $webpage['category_id'];?></td>
			<td><?php echo $webpage['title'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'webpages', 'action' => 'view', $webpage['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'webpages', 'action' => 'edit', $webpage['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'webpages', 'action' => 'delete', $webpage['id']), null, __('Are you sure you want to delete # %s?', $webpage['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>
</div>
