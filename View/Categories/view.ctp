<div class="categories view">
<h2><?php  echo __('Category');?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($category['Category']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($category['Category']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Weight'); ?></dt>
		<dd>
			<?php echo h($category['Category']['weight']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($category['Category']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($category['Category']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="related">
	<h3><?php echo __('Related Webpages');?></h3>
	<?php if (!empty($category['Webpage'])):?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Category Id'); ?></th>
		<th><?php echo __('Title'); ?></th>
		<th><?php echo __('Content'); ?></th>
		<th><?php echo __('Seo Title'); ?></th>
		<th><?php echo __('Seo Description'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th class="actions"><?php echo __('Actions');?></th>
	</tr>
	<?php
		$i = 0;
		foreach ($category['Webpage'] as $webpage): ?>
		<tr>
			<td><?php echo $webpage['id'];?></td>
			<td><?php echo $webpage['category_id'];?></td>
			<td><?php echo $webpage['title'];?></td>
			<td><?php echo $webpage['content'];?></td>
			<td><?php echo $webpage['seo_title'];?></td>
			<td><?php echo $webpage['seo_description'];?></td>
			<td><?php echo $webpage['created'];?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'webpages', 'action' => 'view', $webpage['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'webpages', 'action' => 'edit', $webpage['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'webpages', 'action' => 'delete', $webpage['id']), null, __('Are you sure you want to delete # %s?', $webpage['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>
</div>
