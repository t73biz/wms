<?php

	echo $this->Html->tag(
		'option',
		'(Choose One)',
		array(
			'value' =>	0
		)
	);

	foreach ($categories as $key => $value)
	{
		echo $this->Html->tag(
			'option',
			$value,
			array(
				'value' => $key
			)
		);
	}
?>
