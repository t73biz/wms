<!DOCTYPE html>
<html lang="en">
<head>
	<?php
		echo $this->Html->charset("utf-8");
		echo $this->Html->tag(
			'title',
			__('WMS :: ') . $title_for_layout
		);
		echo $this->Html->meta('icon');
		echo $this->Html->meta('keywords', $keywords_for_layout);
		echo $this->Html->meta('description', $description_for_layout);
		echo $this->Html->css(
			array(
				'reset',
				'text',
				'960',
				'cake',
				'jquery-ui',
				'style',
				'elrte.min',
				'elrte-inner'
			)
		);
		echo $this->Html->script(
			array(
				'jquery',
				'jquery-ui',
				'nav'
			)
		);
		echo $scripts_for_layout;
	?>
<!--[if lte IE 7]>
	<style type="text/css">
		html .jqueryslidemenu{height: 1%;}
	</style>
<![endif]-->
</head>
<body>
	<div class="container_24">
		<?php
			echo $this->element('header');
			echo $this->element('main-nav');
			echo $this->Html->tag('hr',null,array('class' => 'grid_24'));
			echo $this->element('admin-nav');
			echo $this->Session->flash();
		?>
		<div id="content" class="grid_24">
			<?php
				echo $content_for_layout;
			?>
		</div>
		<?php
			echo $this->element('footer');
		?>
	</div>
	<?php
		echo $this->Js->writeBuffer(array('onDomReady' => true));

	?>
</body>
</html>