<?php
App::uses('AppHelper', 'View/Helper');

class NavHelper extends AppHelper
{
	public $helpers = array('Html');
	
	public function build($navArray, $class, $id)
	{
		$nav = "";
		foreach ($navArray as $item)
		{
			$nav .= $this->_buildItem($item);
		}
		$ul = $this->Html->tag('ul',$nav);
		$menu = $this->Html->tag('nav', $ul, array('class' => $class, 'id' => $id));
		return $menu;
	}
	
	private function _buildItem($item)
	{
		if(isset($item['text']))
		{
			$text = $item['text'];
		}
		else
		{
			throw new MalformedNavException();
		}

		if(isset($item['controller']))
		{
			$controller = $item['controller'];
		}
		else
		{
			throw new MalformedNavException();
		}
		
		$action = 'index';
		if(isset($item['action']))
		{
			$action = $item['action'];
		}
		
		$params = '';
		if(isset($item['params']))
		{
			$params = $item['params'];
		}
		
		$subNav = $subMenu = '';
		if(isset($item['submenu']) && is_array($item['submenu']))
		{
			foreach ($item['submenu'] as $item)
			{
				$subMenu .= $this->_buildItem($item);
			}
			$subNav = $this->Html->tag('ul', $subMenu);
		}		
		
		$link = $this->Html->link($text,array('controller' => $controller, 'action' => $action, $params), array('class' => 'grid_4'));
		$listItem = $this->Html->tag('li', $link . $subNav, array('class' => 'grid_4'));
		return $listItem;
	}
}
?>