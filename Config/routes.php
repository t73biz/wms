<?php
	Router::connect('/', array('controller' => 'webpages', 'action' => 'display', 'home'));
	Router::connect('/webpages/:action/*', array('controller' => 'webpages'));
	require APP . 'Config' . DS . 'custom-routes.php';
	CakePlugin::routes();
	require CAKE . 'Config' . DS . 'routes.php';
?>