<?php

App::uses('AppController', 'Controller');

/**
 * Webpages Controller
 *
 * @property Webpage $Webpage
 */
class WebpagesController extends AppController
{

	public $components = array('RequestHandler');

	/**
	 * index method
	 *
	 * @return void
	 */
	public function index()
	{
		$this->Webpage->recursive = 0;
		$this->set('webpages', $this->paginate());
	}

	/**
	 * view method
	 *
	 * @param string $id
	 * @return void
	 */
	public function view($id = null)
	{
		$this->Webpage->id = $id;
		if (!$this->Webpage->exists())
		{
			throw new NotFoundException(__('Invalid webpage'));
		}
		$this->set('webpage', $this->Webpage->read(null, $id));
	}

	/**
	 * add method
	 *
	 * @return void
	 */
	public function add()
	{
		if ($this->request->is('post'))
		{
			$this->Webpage->create();
			if ($this->Webpage->save($this->request->data))
			{
				$this->Session->setFlash(__('The webpage has been saved'));
				$this->_setRoutes();
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The webpage could not be saved. Please, try again.'));
			}
		}
		$categories = $this->Webpage->Category->find('list');
		$keywords = $this->Webpage->Keyword->find('list');
		$this->set(compact('categories', 'keywords'));
	}

	/**
	 * edit method
	 *
	 * @param string $id
	 * @return void
	 */
	public function edit($id = null)
	{
		$this->Webpage->id = $id;
		if (!$this->Webpage->exists())
		{
			throw new NotFoundException(__('Invalid webpage'));
		}
		if ($this->request->is('post') || $this->request->is('put'))
		{
			if ($this->Webpage->save($this->request->data))
			{
				$this->Session->setFlash(__('The webpage has been saved'));
				$this->_setRoutes();
				$this->redirect(array('action' => 'index'));
			}
			else
			{
				$this->Session->setFlash(__('The webpage could not be saved. Please, try again.'));
			}
		}
		else
		{
			$this->request->data = $this->Webpage->read(null, $id);
		}
		$categories = $this->Webpage->Category->find('list');
		$keywords = $this->Webpage->Keyword->find('list');
		$this->set(compact('categories', 'keywords'));
	}

	/**
	 * delete method
	 *
	 * @param string $id
	 * @return void
	 */
	public function delete($id = null)
	{
		if (!$this->request->is('post'))
		{
			throw new MethodNotAllowedException();
		}
		$this->Webpage->id = $id;
		if (!$this->Webpage->exists())
		{
			throw new NotFoundException(__('Invalid webpage'));
		}
		if ($this->Webpage->delete())
		{
			$this->Session->setFlash(__('Webpage deleted'));
			$this->_setRoutes();
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('Webpage was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

	/**
	 * Displays a view
	 *
	 * @param mixed What page to display
	 */
	public function display()
	{
		$path = func_get_args();

		$count = count($path);
		if (!$count)
		{
			$this->redirect('/');
		}

		$page = $this->Webpage->findBySeoTitle($path[0]);

		if (!is_null($page))
		{
			$title_for_layout = $page['Webpage']['title'];
			$this->description_for_layout = $page['Webpage']['seo_description'];
			$this->_setKeywords($page);
			$this->set(compact('page', 'title_for_layout'));
		}
		else
		{
			$this->redirect('/');
		}
	}

	private function _setKeywords($page)
	{
		$words = array();
		foreach ($page['Keyword'] as $keyword)
		{
			$words[] = $keyword['word'];
		}
		$this->keywords_for_layout = implode(",", $words);
	}

	private function _setRoutes()
	{
		$webpages = $this->Webpage->find(
			'all', array('fields' => 'seo_title')
		);
		$routes = "<?php\n";
		foreach ($webpages as $page)
		{
			$routes .= "\tRouter::connect('/" . $page['Webpage']['seo_title'] . "', array('controller' => 'webpages', 'action' => 'display', '" . $page['Webpage']['seo_title'] . "'));\n";
		}
		$routes .= "?>\n";

		$path = APP . 'Config' . DS . 'custom-routes.php';
		$fh = fopen($path, 'w');
		fwrite($fh, $routes);
		fclose($fh);
		$this->_setNav();
	}

	private function _setNav()
	{
		$catList = $this->Webpage->Category->find('list',
			array('order' => 'Category.weight ASC'));
		$nav = "<?php\n";
		$nav .= '$navArray = array(' . EOL;
		foreach ($catList as $id => $name)
		{
			$webpages = $this->Webpage->find(
				'all',
				array(
				'conditions' => array(
					'Webpage.category_id' => $id
				),
				'fields' => array(
					'Webpage.title',
					'Webpage.seo_title'
				),
				'recursive' => -1
				)
			);
			if (!empty($webpages))
			{
				$nav .= TAB . 'array(' . EOL;
				$nav .= TAB . TAB . '"text" => "' . $name . '",' . EOL;
				$nav .= TAB . TAB . '"controller" => "categories",' . EOL;
				$nav .= TAB . TAB . '"action" => "view",' . EOL;
				$nav .= TAB . TAB . '"params" => "' . $id . '",' . EOL;
				$nav .= TAB . TAB . '"submenu" => array(' . EOL;
				foreach ($webpages as $webpage)
				{
					$nav .= TAB . TAB . TAB . 'array(' . EOL;
					$nav .= TAB . TAB . TAB . TAB . '"text" => "' . $webpage['Webpage']['title'] . '",' . EOL;
					$nav .= TAB . TAB . TAB . TAB . '"controller" => "webpages",' . EOL;
					$nav .= TAB . TAB . TAB . TAB . '"action" => "display",' . EOL;
					$nav .= TAB . TAB . TAB . TAB . '"params" => "' . $webpage['Webpage']['seo_title'] . '",' . EOL;
					$nav .= TAB . TAB . TAB . '),' . EOL;
				}
				$nav .= TAB . TAB . ')' . EOL;
				$nav .= TAB . '),' . EOL;
			}
		}
		$nav .= ');' . EOL;
		$nav .= 'echo $this->Nav->build($navArray, "jqueryslidemenu grid_24", "main-nav");' . EOL;
		$nav .= 'echo $this->Js->buffer("jqueryslidemenu.buildmenu(\'main-nav\', arrowimages);");' . EOL;
		$nav .= "?>\n";
		$path = APP . 'View' . DS . 'Elements' . DS . 'main-nav.ctp';
		$fh = fopen($path, 'w');
		fwrite($fh, $nav);
		fclose($fh);
	}

	public function setSeoTitle()
	{
		if ($this->RequestHandler->isAjax())
		{
			$this->set('title',
				strtolower(Inflector::slug($this->params['data']['value'])));
			$this->render('setSeoTitle', 'ajax');
		}
	}

}

?>