<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2011, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2011, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package       app.Controller
 */
class AppController extends Controller
{

	public $components = array('DebugKit.Toolbar', 'Session');
	public $helpers = array('Form', 'Html', 'Js' => array('Jquery'), 'Nav', 'Session');
	public $description_for_layout = "";
	public $keywords_for_layout = "";
	
	public function beforeRender()
	{
		if(empty($this->description_for_layout))
		{
			$this->description_for_layout = "Website Mangement system";
		}
		if(empty($this->keywords_for_layout))
		{
			$this->keywords_for_layout = "website management system, cms, content management system";
		}
		$this->set('description_for_layout',$this->description_for_layout);
		$this->set('keywords_for_layout', $this->keywords_for_layout);
	}
}
